# Routenbuch

Routenbuch is the 100% open source climbing management application for the web.

Use it as a:

* climbing area management database for your local climbing association
* online climbing guide book
* route book to track and plan your climbing activity

## Screenshots

TODO: add screenshot of country overview

## Software stack

- [Ruby on Rails](https://rubyonrails.org/) &mdash; Routenbuch is a ruby on rails application
- [PostgreSQL](https://www.postgresql.org/) &mdash; Database backend
- [Redis](https://redis.io/) &mdash; Used for caching and as job queue for sidekiq
- [Sidekiq](https://github.com/mperham/sidekiq) &mdash; Background processing
- [Bootstrap](https://getbootstrap.com/) &mdash; Frontend toolkit
- [VueJS](https://vuejs.org/) &mdash; Javascript framework for building web interfaces

And a *lots* of Ruby Gems, see [Gemfile](https://gitlab.com/routenbuch/routenbuch/-/blob/master/Gemfile).

## Contributing

Routenbuch is 100% free and open source.

Everyone is encouraged to support and contribute.

Join us on [#routenbuch:matrix.org](https://matrix.to/#/#routenbuch:matrix.org) to discuss and get advice for contribution.

## Copyright / License

Copyright 2016 - 2022 Markus Benning

Routenbuch is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Routenbuch is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Routenbuch.  If not, see <http://www.gnu.org/licenses/>.
