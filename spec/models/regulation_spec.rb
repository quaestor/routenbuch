# == Schema Information
#
# Table name: regulations
#
#  id         :bigint           not null, primary key
#  abbr       :string
#  body       :text
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  geo_ref_id :bigint
#
# Indexes
#
#  index_regulations_on_geo_ref_id  (geo_ref_id)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#
require 'rails_helper'

RSpec.describe Regulation, type: :model do
  let(:geo_ref_factory) { :region }
  let(:geo_ref) { create(geo_ref_factory) }
  let(:attributes) do
    {
      geo_ref: geo_ref
    }
  end
  let(:regulation) { create :regulation, **attributes }

  context 'regulation' do
    it 'validation' do
      expect { regulation.validate! }.not_to raise_error
    end

    it 'associations' do
      expect(regulation.regulated_geo_refs).to all(be_an(RegulatedGeoRef))
      expect(regulation.regulated_geo_refs.count).to be == 3
    end

    context 'without name' do
      let(:attributes) { { name: '' } }

      it 'validation' do
        expect { regulation.validate! }.to raise_error ActiveRecord::RecordInvalid
      end
    end

    %i[crag sector].each do |type|
      context "with geo_ref of type #{type}" do
        let(:geo_ref_factory) { type }

        it 'validation' do
          expect { regulation.validate! }.to raise_error /must be a country or region/
        end
      end
    end
  end
end
