require 'rails_helper'

RSpec.describe UserRating, type: :model do
  let(:attributes) { {} }
  let(:factory) { :user_rating }
  let(:user_rating) { create factory, **attributes }
  let(:rating) { user_rating.rating }

  context 'object creation' do
    it 'validation' do
      expect { user_rating.validate! }.not_to raise_error
    end

    it 'updates rating average after create' do
      expect do
        create(:user_rating, rating: rating, score: 1)
      end.not_to raise_error
      expect(rating.average_score).to eq 3.6
      expect do
        create(:user_rating, rating: rating, score: 1)
      end.not_to raise_error
      expect(rating.average_score).to eq 3.1666666666666665
    end

    it 'updates rating after destroy' do
      expect do
        rating.user_ratings.destroy_all
      end.not_to raise_error
      expect(rating.average_score).to eq nil
    end
  end
end
