# == Schema Information
#
# Table name: geo_refs
#
#  id                :integer          not null, primary key
#  access            :string           default("internal"), not null
#  alternative_names :string           default([]), not null, is an Array
#  body              :text
#  description       :string
#  height            :integer
#  lat               :decimal(10, 6)
#  legal_status      :string
#  lng               :decimal(10, 6)
#  name              :string
#  orientation       :integer
#  priority          :integer          default(0), not null
#  searchable_terms  :string           default(""), not null
#  stats             :json
#  type              :string           default("Crag"), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  parent_id         :integer
#  zone_id           :integer
#
# Indexes
#
#  index_geo_refs_on_access        (access)
#  index_geo_refs_on_lat_and_lng   (lat,lng)
#  index_geo_refs_on_legal_status  (legal_status)
#  index_geo_refs_on_parent_id     (parent_id)
#  index_geo_refs_on_priority      (priority)
#  index_geo_refs_on_type          (type)
#  index_geo_refs_on_zone_id       (zone_id)
#
# Foreign Keys
#
#  fk_rails_...  (zone_id => zones.id)
#
require 'rails_helper'

RSpec.describe GeoRef, type: :model do
  let(:attributes) { {} }
  let(:factory) { :geo_ref }
  let(:geo_ref) { create factory, **attributes }

  context 'object creation' do
    it 'validation' do
      expect { geo_ref.validate! }.not_to raise_error
    end
  end

  context 'basic crag' do
    let(:factory) { :weißenstein }

    it 'attributes' do
      expect(geo_ref.height).to eq 15
      expect(geo_ref.orientation).to eq 180
    end

    it 'rbid' do
      expect(geo_ref.rbids.first).to be_a Rbid
    end

    it 'associations' do
      expect(geo_ref.zone).to be_a Zone
      expect(geo_ref.routes.first).to be_a Route
    end

    it 'support flags' do
      %i[
        orientation
        height
        regulation
        ascents
        ticklists
        paths
        approaches
        regulations
        closures
      ].each do |flag|
        expect(geo_ref).to respond_to "supports_#{flag}?"
      end
    end
  end

  context 'statistics' do
    let(:factory) { :weißenstein }

    before do
      geo_ref.update_stats
      geo_ref.save!
      geo_ref.reload
    end

    it 'route grade statistics' do
      expect(geo_ref.route_grade_stats).to eq(
        {
          'easy' => 1,
          'extrem' => 1,
          'hard' => 1,
          'medium' => 1,
          'ultra' => 1,
          'very-easy' => 1
        }
      )
    end

    it 'statistics' do
      expect(geo_ref.stats).to eq(
        {
          'childs_count' => 0,
          'descendent_crags_count' => 0,
          'descendent_routes_count' => 6,
          'descendents_count' => 0,
          'route_grade_stats' => {
            'very-easy' => 1,
            'easy' => 1,
            'medium' => 1,
            'hard' => 1,
            'extrem' => 1,
            'ultra' => 1
          }
        }
      )
      # also check order of route_grade_stats
      expect(geo_ref.stats['route_grade_stats'].keys).to eq(
        ['very-easy', 'easy', 'medium', 'hard', 'extrem', 'ultra']
      )
    end

    it 'update in background' do
      expect {
        geo_ref.update_self_and_parents_stats
      }.to have_enqueued_job.with(geo_ref.id)
    end
  end

  context 'with parents' do
    let(:factory) { :weißenstein }

    it 'parents' do
      expect(geo_ref.parents.pluck(:name)).to eq ['Frankenjura', 'Germany']
    end
  end

  context 'with hierarchy' do
    let(:factory) { :weißenstein }

    before do
      geo_ref
      @country = GeoRef.find_by! name: 'Germany'
    end

    it 'childs' do
      expect(@country.childs.pluck(:name)).to eq ['Frankenjura']
    end

    it 'descendents' do
      expect(@country.descendents.pluck(:name)).to \
        eq ['Frankenjura', 'Weißenstein']
      expect(@country.self_and_descendents.pluck(:name)).to \
        eq ['Germany', 'Frankenjura', 'Weißenstein']
    end
  end
end
