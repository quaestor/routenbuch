# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  access                 :string           default("private"), not null
#  ascents_access         :string           default("private"), not null
#  bio                    :text
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  locale                 :string
#  name                   :string
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :string           not null
#  scope_embed_api        :boolean          default(FALSE), not null
#  scope_web_ui           :boolean          default(TRUE), not null
#  settings               :jsonb
#  sign_in_count          :integer          default(0), not null
#  stats                  :json
#  ticklists_access       :string           default("private"), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  home_geo_ref_id        :bigint
#
# Indexes
#
#  index_users_on_access                (access)
#  index_users_on_ascents_access        (ascents_access)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_home_geo_ref_id       (home_geo_ref_id)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_ticklists_access      (ticklists_access)
#
# Foreign Keys
#
#  fk_rails_...  (home_geo_ref_id => geo_refs.id)
#
require 'rails_helper'

RSpec.describe User, type: :model do
  let(:attributes) { {} }
  let(:user) { create :user, **attributes }

  context 'user' do
    it 'validation' do
      expect { user.validate! }.not_to raise_error
    end

    context 'with weak password' do
      let(:attributes) { { password: 'weak1234' } }

      it 'validation' do
        expect { user.validate! }.to raise_error /Password is too weak/
      end
    end
  end
end
