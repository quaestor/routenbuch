# == Schema Information
#
# Table name: season_closures
#
#  id                 :bigint           not null, primary key
#  description        :string
#  end_day_of_month   :integer
#  end_month          :integer
#  start_day_of_month :integer
#  start_month        :integer
#  year               :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  closure_id         :bigint           not null
#
# Indexes
#
#  index_season_closures_on_closure_id           (closure_id)
#  index_season_closures_on_closure_id_and_year  (closure_id,year) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (closure_id => closures.id)
#
require 'rails_helper'

RSpec.describe SeasonClosure, type: :model do
  let(:attributes) { {} }
  let(:season_closure) { create :season_closure, **attributes }

  context 'season_closure' do
    it 'validation' do
      expect { season_closure.validate! }.not_to raise_error
    end
  end
end
