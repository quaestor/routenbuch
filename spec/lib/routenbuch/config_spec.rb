require 'rails_helper'

RSpec.describe Routenbuch::Config do
  let(:config_file) { Rails.root.join('spec/fixtures/files/config.yaml') }
  let(:config) do
    config = Routenbuch::Config.new
    config.load_file(config_file)
    config
  end

  it 'initialize' do
    expect { config }.not_to raise_error
  end

  it 'keys' do
    expect(config.value3).to eq 'zumsel'
    expect(config['value3']).to eq 'zumsel'
    expect(config[:value3]).to eq 'zumsel'
  end

  it 'sections' do
    expect(config.section_a).to be_a Routenbuch::Config
    expect(config.section_a['value1']).to eq 'bla'
  end
end
