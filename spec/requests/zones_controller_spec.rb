require 'rails_helper'

RSpec.describe ZonesController, type: :request do
  login_user :admin_user

  it 'index' do
    get zones_path
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response).to have_http_status(:ok)
  end
end
