require 'rails_helper'

RSpec.describe SearchController, type: :request do
  login_user :admin_user

  it 'index' do
    get search_index_path, params: { q: 'test' }
    expect(response).to have_http_status(:ok)
  end
end
