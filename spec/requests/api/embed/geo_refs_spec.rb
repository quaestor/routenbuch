require 'swagger_helper'

RSpec.describe 'api/embed/v1', type: :request do
  let(:user_factory) { :admin_user }
  let(:user) { create(user_factory) }
  let(:Authorization) do
    ActionController::HttpAuthentication::Basic.encode_credentials(
      user.email, user.password
    )
  end

  before(:each) do
    create_list(:closure, 3)
  end

  after do |example|
    example.metadata[:response][:content] = {
      'application/json' => {
        example: JSON.parse(response.body, symbolize_names: true)
      }
    }
  end

  path '/api/embed/v1/geo_refs/with_closures' do
    get('retrieve locations with closures') do
      tags 'Locations'
      security [ basic_auth: [] ]
      produces 'application/json'

      response(200, 'successful') do
        schema(
          jsonapi_collection(
            '$ref': '#/components/schemas/geo_ref'
          )
        )

        run_test!
      end
    end
  end
end
