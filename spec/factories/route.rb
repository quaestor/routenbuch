FactoryBot.define do
  factory :route do
    name { 'Frankenschnellweg' }
    geo_ref factory: :crag
    grade

    factory :route_very_easy do
      name { 'Dampfhammer' }
      grade factory: :grade_very_easy
    end

    factory :route_easy do
      name { 'Entsafter' }
      grade factory: :grade_easy
    end

    factory :route_medium do
      name { 'Wilde 13' }
      grade factory: :grade_medium
    end

    factory :route_hard do
      name { 'Krampfhammer' }
      grade factory: :grade_hard
    end

    factory :route_extrem do
      name { 'Saftpresse' }
      grade factory: :grade_extrem
    end

    factory :route_ultra do
      name { 'Panische Zeiten' }
      grade factory: :grade_ultra
    end
  end
end
