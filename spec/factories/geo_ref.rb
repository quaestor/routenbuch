FactoryBot.define do
  factory :country, class: Country do
    name { 'Germany' }
  end

  factory :region, class: Region do
    name { 'Frankenjura' }
    parent factory: :country
  end

  factory :crag, class: Crag, aliases: [:geo_ref] do
    name { 'Rodenstein' }
    parent factory: :region
  end

  factory :sector, class: Sector do
    name { 'Rodenstein - Edelweißpfeiler' }
    parent factory: :crag
  end

  factory :weißenstein, class: Crag do
    parent factory: :region
    name { 'Weißenstein' }
    height { 15 }
    orientation { 180 }
    zone
    after(:create) do |crag, evaluator|
      %i[
        route_very_easy
        route_easy
        route_medium
        route_hard
        route_extrem
        route_ultra
      ].each { |f| create(f, geo_ref: crag) }
      crag.update_stats
      crag.save!
    end
  end
end
