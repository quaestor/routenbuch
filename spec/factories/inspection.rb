FactoryBot.define do
  factory :inspection do
    send(:when) { 3.days.ago }
    body { lorem_ipsum }
    regulated_geo_ref
  end
end
