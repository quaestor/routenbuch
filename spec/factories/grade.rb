FactoryBot.define do
  factory :grade, aliases: [:grade_hard] do
    scale { 'UIAA' }
    scope { 'free_climbing' }
    grade { '8+' }
    difficulty { 15 }
    category { 'hard' }

    factory :grade_very_easy do
      grade { '4' }
      difficulty { 5 }
      category { 'very-easy' }
    end

    factory :grade_easy do
      grade { '6' }
      difficulty { 7 }
      category { 'easy' }
    end

    factory :grade_medium do
      grade { '7' }
      difficulty { 11 }
      category { 'medium' }
    end

    factory :grade_extrem do
      grade { '9' }
      difficulty { 19 }
      category { 'extrem' }
    end

    factory :grade_ultra do
      grade { '11' }
      difficulty {  25 }
      category { 'ultra' }
    end
  end
end
