FactoryBot.define do
  factory :comment, aliases: [:geo_ref_comment] do
    body { '<p>Hello World!</p>' }
    target factory: :crag
    user
    geo_ref do
      target.is_a?(GeoRef) ? target : target.geo_ref
    end

    factory :route_comment, parent: :comment do
      target factory: :route
    end
  end
end
