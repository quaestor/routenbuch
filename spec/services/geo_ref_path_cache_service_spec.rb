require 'rails_helper'

RSpec.describe GeoRefPathCacheService do
  before do
    @sector = create(:sector)
    @crag = @sector.parent
    @region = @crag.parent
    @country = @region.parent
    @geo_refs = [@country, @region, @crag, @sector]
  end

  def all_geo_ref_path_caches
    GeoRefPathCache.where(
      geo_ref: @country.self_and_descendents
    )
  end

  it 'path_cache must be empty by default' do
    expect(all_geo_ref_path_caches.any?).to be false
  end

  it '#update' do
    expect do
      GeoRefPathCacheService.update(@country)
    end.not_to raise_error
    @geo_refs.each do |geo_ref|
      expect(geo_ref.path_cache).to be_present
      expect(geo_ref.path_cache.path).to be_a Array
      expect(geo_ref.path_cache.path.size).to eq(geo_ref.parents.count + 1)
    end
  end

  it '#clear' do
    GeoRefPathCacheService.update(@country)

    expect do
      GeoRefPathCacheService.clear(@country)
    end.not_to raise_error
    expect(all_geo_ref_path_caches.any?).to be false
  end
end
