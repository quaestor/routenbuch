require 'legal_status_type'
require 'access_level_type'

ActiveRecord::Type.register(:legal_status, LegalStatusType)
ActiveRecord::Type.register(:access_level, AccessLevelType)
