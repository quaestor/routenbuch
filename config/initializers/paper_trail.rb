PaperTrail.config.enabled = true
PaperTrail.config.has_paper_trail_defaults = {
  # use our custom version model
  versions: {
    class_name: 'Version'
  }
}
