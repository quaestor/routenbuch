class UserUpdateStatsJob < ApplicationJob
  queue_as :update_stats

  def perform(user_id)
    user = User.find(user_id)
    return unless user

    user.update_stats
    user.save!
  end
end
