require 'uri'

class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value.blank? || URI::regexp.match(value)
      record.errors.add(attribute, _("is not a valid URL"))
    end
  end
end
