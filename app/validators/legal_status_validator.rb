class LegalStatusValidator < ActiveModel::Validations::InclusionValidator
  def options
    {
      in: LegalStatusType.valid_values,
      allow_blank: true
    }
  end
end
