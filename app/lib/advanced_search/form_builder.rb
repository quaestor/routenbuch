module AdvancedSearch
  class FormBuilder < Ransack::Helpers::FormBuilder
    def fields_of_type(type, &block)
      new_object = object.send("build_#{type}")
      name = "#{type}_fields"
      send(name, new_object, child_index: "new_#{type}", &block)
    end

    def button_label
      {
        value: _('Add Value'),
        condition: _('Add Condition'),
        sort: _('Add Sort'),
        grouping: _('Add Condition Group')
      }.freeze
    end

    def sort_attribute_select(options = {}, html_options = {})
      attribute_select(options, html_options, 'sort'.freeze)
    end

    def searchable_associations
      @object.context.searchable_associations
    end
  end
end
