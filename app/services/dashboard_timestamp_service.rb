# manage timestamps for the dashboard
#
# @example Basic usage
#   ds = DashboardTimestampService.new(User.find(1))
#
#   # when did the user mark comments as read?
#   ds.timestamp_for('comments')
#   # => Mon, 27 Jun 2022 19:54:33 +0000
#
#   # mark comments as read / update timestamp
#   ds.mark_as_read('comments')
#   # or mark everything as read
#   ds.mark_all_as_read
class DashboardTimestampService
  # owning user of the timestamps to query
  #
  # @return [User]
  attr_reader :user

  # @param [User] user for which to manage timestamps
  def initialize(user)
    @user = user
  end

  # lazy loads all timestamps of a user indexed by `kind`
  #
  # @return [Hash[String, Time]] map of users timestamps
  def timestamps
    return @timestamps if defined?(@timestamps)
    return @timestamps = {} if user.nil?

    @timestamps = user.dashboard_timestamps.map do |ts|
      [ts.kind, ts.updated_at]
    end.to_h
  end

  # Get a timestamp of last 'mark as read' for a specific kind
  #
  # @param [String] kind of timestamp
  # @return [Time] timestamp of last 'mark as read'
  def timestamp_for(kind)
    ts = timestamps.values_at(kind, 'all').reject(&:nil?).max
    return ts unless ts.nil?

    14.days.ago
  end

  # update a timestamp to mark it as read
  #
  # @param [String] kind of timestamp
  def mark_as_read(kind)
    now = Time.now
    if can_set?
      DashboardTimestamp.upsert_all(
        [
          { user_id: user.id, kind: kind, updated_at: now }
        ],
        unique_by: %i[user_id kind]
      )
    end
    timestamps[kind] = now
  end

  # update wildcard timestamp
  def mark_all_as_read
    mark_as_read('all')
  end

  # is it possible to set timestamps?
  def can_set?
    return true if user&.persisted?

    false
  end
end
