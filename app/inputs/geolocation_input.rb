class GeolocationInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    lat_field = options[:lat_field] || 'lat'
    lng_field = options[:lat_field] || 'lng'
    lat_input_name = "#{@builder.object_name}[#{lat_field}]"
    lng_input_name = "#{@builder.object_name}[#{lng_field}]"
    initial_lat = @builder.object.send(lat_field)
    initial_lng = @builder.object.send(lng_field)

    tag_attr = {
      'lat-input-name': lat_input_name,
      'lng-input-name': lng_input_name,
      'initial-lat': initial_lat,
      'initial-lng': initial_lng,
    }

    if initial_lat.nil? \
        && initial_lng.nil? \
        && @builder.object.respond_to?(:next_location) \
        && (next_location = @builder.object.send(:next_location))
      tag_attr.merge!(
        'center-lat': next_location.lat,
        'center-lng': next_location.lng
      )
    end

    template.content_tag(
      'geolocation-input',
      '',
      **tag_attr
    ).html_safe
  end
end
