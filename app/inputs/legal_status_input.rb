class LegalStatusInput < SimpleForm::Inputs::CollectionSelectInput
  def collection
    LegalStatusType.valid_values.map do |value|
      [_(value), value]
    end
  end
end
