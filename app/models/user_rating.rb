# == Schema Information
#
# Table name: user_ratings
#
#  id        :bigint           not null, primary key
#  score     :integer          not null
#  rating_id :bigint           not null
#  user_id   :bigint           not null
#
# Indexes
#
#  index_user_ratings_on_rating_id_and_user_id  (rating_id,user_id) UNIQUE
#
class UserRating < ApplicationRecord
  def self.icon
    'star'
  end

  belongs_to :rating, counter_cache: true
  belongs_to :user

  validates :score, presence: true, score: true
  validates(
    :rating,
    uniqueness: {
      scope: :user,
      message: 'can only be set once per user'
    }
  )

  after_save :update_rating_average_score_after_change
  after_destroy :update_rating_average_score_after_change
  def update_rating_average_score_after_change
    rating.update_average_score if saved_change_to_score? || destroyed?
  end
end
