# == Schema Information
#
# Table name: rbids
#
#  id         :bigint           not null, primary key
#  hint       :string           default(""), not null
#  item_type  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  item_id    :bigint           not null
#
# Indexes
#
#  index_rbids_on_item  (item_type,item_id)
#
require 'routenbuch/identifier'

class Rbid < ApplicationRecord
  belongs_to :item, polymorphic: true

  def rbid
    Routenbuch::Identifier.encode(id)
  end

  def self.find_by_rbid(rbid)
    id = Routenbuch::Identifier.decode(rbid.gsub(/^#/, ''))
    find(id)
  end

  def icon
    'link'
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[hint]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end
end
