# == Schema Information
#
# Table name: versions
#
#  id             :bigint           not null, primary key
#  event          :string           not null
#  item_type      :string           not null
#  object         :jsonb
#  object_changes :jsonb
#  whodunnit      :string
#  created_at     :datetime
#  geo_ref_id     :bigint
#  item_id        :bigint           not null
#  route_id       :bigint
#  user_id        :bigint
#
# Indexes
#
#  index_versions_on_geo_ref_id             (geo_ref_id)
#  index_versions_on_item_type_and_item_id  (item_type,item_id)
#  index_versions_on_route_id               (route_id)
#  index_versions_on_user_id                (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id) ON DELETE => cascade
#  fk_rails_...  (route_id => routes.id) ON DELETE => nullify
#  fk_rails_...  (user_id => users.id) ON DELETE => nullify
#
class Version < ApplicationRecord
  include PaperTrail::VersionConcern

  # our routes should be associated with the tree in addtion
  # to the changed item which may be a sub-item of a route (inventory)
  # or a geo_ref (closure)
  belongs_to :geo_ref, optional: true
  validates :geo_ref, presence: true, unless: :standalone_version?

  belongs_to :route, optional: true

  # direct reference to the user object if available
  belongs_to :user, optional: true

  # object is not directly assigned to a node in the tree hierarchy
  def standalone_version?
    item.is_a? FirstAscentPerson
  end

  def self.icon
    'history'
  end
end
