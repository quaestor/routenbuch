# == Schema Information
#
# Table name: effective_permissions
#
#  geo_ref_id    :bigint
#  permission_id :bigint
#
# Indexes
#
#  index_effective_permissions_on_geo_ref_id                    (geo_ref_id)
#  index_effective_permissions_on_geo_ref_id_and_permission_id  (geo_ref_id,permission_id) UNIQUE
#  index_effective_permissions_on_permission_id                 (permission_id)
#
class EffectivePermission < ApplicationRecord
  belongs_to :geo_ref
  belongs_to :permission
end
