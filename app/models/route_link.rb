# == Schema Information
#
# Table name: route_links
#
#  id                  :bigint           not null, primary key
#  anchor_number       :integer
#  description         :string
#  first_anchor_number :integer
#  kind                :string           default("belay"), not null
#  last_anchor_number  :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  route_from_id       :bigint
#  route_to_id         :bigint
#
# Indexes
#
#  index_route_links_on_route_from_id  (route_from_id)
#  index_route_links_on_route_to_id    (route_to_id)
#
# Foreign Keys
#
#  fk_rails_...  (route_from_id => routes.id)
#  fk_rails_...  (route_to_id => routes.id)
#
class RouteLink < ApplicationRecord
  class << self
    KIND_DESCRIPTIONS = {
      N_('belay') => N_('Shares a common belay with another route'),
      N_('start_with') => N_('Uses the start of another route'),
      N_('end_with') => N_('Uses the end of another route'),
      N_('overlap') => N_('Overlaps with another route'),
      N_('cross') => N_('Crosses another route'),
    }.freeze

    KINDS = KIND_DESCRIPTIONS.keys.freeze 

    def kind_descriptions
      KIND_DESCRIPTIONS
    end

    def kinds
      KINDS
    end

    def valid_kind?(kind)
      KINDS.include? kind
    end

    def icon
        'random'
    end
  end

  belongs_to :route_from, class_name: 'Route', foreign_key: 'route_from_id'
  belongs_to :route_to, class_name: 'Route', foreign_key: 'route_to_id'

  validates :kind, presence: true
  validate :validate_kind
  def validate_kind
    return if kind.blank?
    return if self.class.valid_kind? kind

    errors.add(:kind, _('is not a valid kind'))
  end

  validates :anchor_number, numericality: { only_integer: true, allow_nil: true }
  validates :first_anchor_number, numericality: { only_integer: true, allow_nil: true }
  validates :last_anchor_number, numericality: { only_integer: true, allow_nil: true }

  validate :validate_route_parents
  # check if routes are on same crag or parent(when sector)
  def validate_route_parents
    return unless route_to.present? && route_from.present?
    return if route_from.geo_ref.parent_crag == route_to.geo_ref.parent_crag

    errors.add(:route_to, _('must be on the same crag'))
  end

  validate :validate_route_self_link
  # validate that route does not linkt to itself
  def validate_route_self_link
    return unless route_to.present? && route_from.present?
    return if route_to != route_from

    errors.add(:route_to, _('cannot be linked to itself'))
  end

  def supports_first_anchor_number?
    %w[end_with cross overlap].include? kind
  end

  def supports_last_anchor_number?
    %w[start_with overlap].include? kind
  end

  validates(
    :first_anchor_number,
    absence: true,
    unless: :supports_first_anchor_number?
  )
  validates(
    :last_anchor_number,
    absence: true,
    unless: :supports_last_anchor_number?
  )

  def inventories
    query = \
    case kind
    when 'belay'
      route_to.inventories.where(role: 'belay')
    when 'cross'
      route_to.inventories.where(
        role: 'intermediate anchor',
        anchor_number: first_anchor_number
      ) if first_anchor_number.present?
    when 'start_with'
      route_to.inventories.where(
        'anchor_number <= ?', last_anchor_number
      ) if last_anchor_number.present?
    when 'end_with'
      route_to.inventories.where(
        'anchor_number >= ?', first_anchor_number
      ) if first_anchor_number.present?
    when 'overlap'
      route_to.inventories.where(
        'anchor_number >= ? AND anchor_number <= ?',
        first_anchor_number,
        last_anchor_number
      ) if first_anchor_number.present? && last_anchor_number.present?
    end

    return Inventory.none if query.nil?

    query
  end

  has_paper_trail(
    meta: {
      route_id: :route_from_id,
      geo_ref_id: proc { |l| l.route_from.geo_ref_id }
    }
  )
end
