module Taggable
  extend ActiveSupport::Concern

  included do
    has_many :assigned_tags, as: :taggable
    has_many :tags, through: :assigned_tags

    has_many :supported_tags, class_name: 'Tag', primary_key: :type, foreign_key: :model
  end

  def add_tag(tag_or_name)
    assigned_tags.find_or_create_by!(
      tag: find_tag_by_tag_or_name!(tag_or_name)
    )
  end

  def remove_tag(tag_or_name)
    assigned_tags.where(
      tag: find_tag_by_tag_or_name!(tag_or_name)
    ).delete_all
  end

  def supports_tag?(name)
    supported_tags.where(name: name).exists?
  end

  private

  def find_tag_by_tag_or_name!(tag_or_name)
    if tag_or_name.instance_of? Tag
      tag_or_name
    else
      Tag.find_by!(model: self.class.name, name: tag_or_name)
    end
  end
end
