# == Schema Information
#
# Table name: closure_templates
#
#  id                         :bigint           not null, primary key
#  name                       :string           not null
#  regular_end_day_of_month   :integer
#  regular_end_month          :integer
#  regular_start_day_of_month :integer
#  regular_start_month        :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  closure_reason_id          :bigint
#
# Indexes
#
#  index_closure_templates_on_closure_reason_id  (closure_reason_id)
#
# Foreign Keys
#
#  fk_rails_...  (closure_reason_id => closure_reasons.id)
#
class ClosureTemplate < ApplicationRecord
  validates(
    :name,
    presence: true,
    uniqueness: { message: _('must be unique') }
  )
  validates(
    :regular_start_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  )
  validates(
    :regular_start_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  )
  validates(
    :regular_end_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  )
  validates(
    :regular_end_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  )
  belongs_to :closure_reason, optional: true
end
