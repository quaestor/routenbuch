# == Schema Information
#
# Table name: assigned_tags
#
#  taggable_type :string
#  tag_id        :bigint
#  taggable_id   :bigint
#
# Indexes
#
#  index_assigned_tags_on_tag_id                                    (tag_id)
#  index_assigned_tags_on_taggable_type_and_taggable_id             (taggable_type,taggable_id)
#  index_assigned_tags_on_taggable_type_and_taggable_id_and_tag_id  (taggable_type,taggable_id,tag_id) UNIQUE
#
class AssignedTag < ApplicationRecord
  belongs_to :taggable, polymorphic: true
  belongs_to :tag

  def self.ransackable_attributes(_auth_object = nil)
    %w[]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end

  validate :validate_model
  def validate_model
    return if tag.model == taggable.class.name

    errors.add(:tag, 'cannot be assigned to this model')
  end

  validates(
    :tag,
    uniqueness: {
      scope: :taggable,
      message: 'can only be assigned once'
    }
  )
end
