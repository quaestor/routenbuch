# == Schema Information
#
# Table name: tags
#
#  id          :bigint           not null, primary key
#  category    :string           default("other"), not null
#  description :string
#  icon        :string
#  model       :string           default("Crag"), not null
#  name        :string
#  priority    :integer          default(10), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_tags_on_model           (model)
#  index_tags_on_model_and_name  (model,name) UNIQUE
#  index_tags_on_name            (name)
#
class Tag < ApplicationRecord
  has_many :assigned_tags
  has_many :taggables, through: :assigned_tags

  validates :name, :model, :description, presence: true
  validates(
    :name,
    uniqueness: {
      scope: :model,
      message: "tags can only exist once per model"
    }
  )

  def self.ransackable_attributes(_auth_object = nil)
    %w[name description]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end
end
