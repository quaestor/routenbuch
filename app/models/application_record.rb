class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # force whitelisting of ransack attributes, associations, ...
  class << self
    def ransackable_attributes(_auth_object = nil)
      %w[name description]
    end

    def ransackable_associations(_auth_object = nil)
      []
    end

    def ransackable_scopes(_auth_object = nil)
      []
    end

    def ransack_attribute_info
      info = ransackable_attributes.map do |name|
        [name, attribute_types[name].type]
      end.to_h

      ransackable_associations.each do |association|
        reflection = reflect_on_association(association)
        next unless reflection

        klass = reflection.klass
        info[association] = klass.ransackable_attributes.map do |name|
          [name, klass.attribute_types[name].type]
        end.to_h
      end

      info
    end
  end

  def as_base_class
    return self if self.class.base_class?

    return self.becomes(self.class.base_class)
  end

  def self.icon
    'question'
  end

  def icon
    self.class.icon
  end

  def display_name
    try(:name) || id.to_s
  end

  def self.display_name
    name
  end
end
