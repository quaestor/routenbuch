# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  access                 :string           default("private"), not null
#  ascents_access         :string           default("private"), not null
#  bio                    :text
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  locale                 :string
#  name                   :string
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :string           not null
#  scope_embed_api        :boolean          default(FALSE), not null
#  scope_web_ui           :boolean          default(TRUE), not null
#  settings               :jsonb
#  sign_in_count          :integer          default(0), not null
#  stats                  :json
#  ticklists_access       :string           default("private"), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  home_geo_ref_id        :bigint
#
# Indexes
#
#  index_users_on_access                (access)
#  index_users_on_ascents_access        (ascents_access)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_home_geo_ref_id       (home_geo_ref_id)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_ticklists_access      (ticklists_access)
#
# Foreign Keys
#
#  fk_rails_...  (home_geo_ref_id => geo_refs.id)
#
class User < ApplicationRecord
  class << self
    ACCESS_LEVELS = [
      N_('private'),
      N_('internal'),
      *(N_('public') if Routenbuch.public?)
    ].freeze

    def access_levels
      ACCESS_LEVELS
    end
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise_modules = %i[
    database_authenticatable
    recoverable
    rememberable
    trackable
    validatable
  ]
  devise_modules << :registerable if Routenbuch.sign_up?
  devise(*devise_modules)

  validates :access, presence: true, access_level: true
  validates :ascents_access, presence: true, access_level: true
  validates :ticklists_access, presence: true, access_level: true

  def valid_access_levels
    self.class.access_levels
  end

  def is_valid_access_level?(level)
    valid_access_levels.include?(level)
  end

  has_many :ascents
  has_many :ticklists
  has_many :photos
  has_many :permissions

  has_many :authored_versions, class_name: 'Version', dependent: :nullify

  belongs_to :home_geo_ref, class_name: 'GeoRef', optional: true
  has_many :dashboard_timestamps

  ROLES = %w[admin contributor member guest].freeze

  def valid_roles
    ROLES
  end

  validates(
    :password,
    password_strength: true,
    unless: proc { |u| u.password.blank? }
  )

  validate :validate_role
  def validate_role
    return if valid_roles.include? role

    errors.add(:role, _('must be one of %{roles}') % { roles: valid_roles.join(', ') })
  end

  after_initialize :init
  def init
    self.role ||= 'member'
  end

  def self.icon
    'users'
  end

  def icon
    'user'
  end

  def display_name
    if name.nil? || name.blank?
      "User #{id}"
    else
      name
    end
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[name role]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[]
  end

  def self.ransackable_scopes(_auth_object = nil)
    %i[search_full_text]
  end

  def update_stats
    self.stats = UserPresenters::StatsPresenter.new(self).stats
  end

  def after_ascent_commit(ascent)
    UserUpdateStatsJob.perform_later(id)
  end

  def last_ascent
    ascents.order('ascents.when DESC').first
  end

  def access_scopes
    scopes = []
    scopes << 'web_ui' if scope_web_ui?
    scopes << 'embed_api' if scope_embed_api?
    scopes
  end

  has_one_attached :avatar
  validates(
    :avatar,
    content_type: Routenbuch.supported_image_types,
    size: { less_than: Routenbuch.max_upload_size }
  )

  validate :validate_locale
  def validate_locale
    return if locale.blank?
    return if Routenbuch.locales.include?(locale.to_sym)

    errors.add(:locale, _('must be one of: %{values}') % { values: Routenbuch.locales.join(', ') })
  end

  jsonb_accessor(
    :settings,
    theme: [:string, { default: 'twentytwo' }]
  )

  validates :theme, user_theme: true, presence: true

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      name: 'A',
      email: 'A'
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )

  def initial_letters
    return unless name.present?

    name.split.map(&:first).join.upcase[0..1]
  end
end
