# == Schema Information
#
# Table name: regulations
#
#  id         :bigint           not null, primary key
#  abbr       :string
#  body       :text
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  geo_ref_id :bigint
#
# Indexes
#
#  index_regulations_on_geo_ref_id  (geo_ref_id)
#
# Foreign Keys
#
#  fk_rails_...  (geo_ref_id => geo_refs.id)
#
class Regulation < ApplicationRecord
  class << self
    def valid_geo_ref_types
      [Country, Region].freeze
    end

    def valid_geo_ref_type?(klass)
      valid_geo_ref_types.include? klass
    end

    def icon
      'file-text-o'
    end
  end

  belongs_to :geo_ref
  has_many :regulated_geo_refs, dependent: :destroy

  validates :name, presence: true

  validate :validate_geo_ref_type
  def validate_geo_ref_type
    return if self.class.valid_geo_ref_type? geo_ref.class

    errors.add(:geo_ref, _('must be a country or region'))
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[name abbr body]
  end

  def self.ransackable_associations(_auth_object = nil)
    []
  end

  def self.ransackable_scopes(_auth_object = nil)
    %i[search_full_text]
  end

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      name: 'A',
      abbr: 'B',
      body: 'C'
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )

  has_paper_trail(
    meta: {
      geo_ref_id: :geo_ref_id
    }
  )
end
