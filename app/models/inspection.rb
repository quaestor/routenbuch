# == Schema Information
#
# Table name: inspections
#
#  id                   :bigint           not null, primary key
#  body                 :text
#  when                 :date
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  regulated_geo_ref_id :bigint
#
# Indexes
#
#  index_inspections_on_regulated_geo_ref_id  (regulated_geo_ref_id)
#
class Inspection < ApplicationRecord
  belongs_to :regulated_geo_ref

  def self.ransackable_attributes(_auth_object = nil)
    %w[body]
  end

  def self.ransackable_associations(_auth_object = nil)
    []
  end

  validates :when, presence: true

  has_many_attached :protocols

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      body: 'A'
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )
end
