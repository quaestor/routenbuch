class GeoRef
  class MapItemSerializer
    include JSONAPI::Serializer

    set_type :geo_ref
    set_id :id
    
    attributes :name, :lat, :lng, :type

    link :url do |object|
      Rails.application.routes.url_helpers.geo_ref_path(object)
    end
  end
end
