class ToposController < ApplicationController
  before_action :set_topo, only: [:show, :edit, :update, :destroy]
  skip_authorization_check :only => [:index]

  def index
    @q = Topo.ransack(params[:q])
    @topos = @q.result(distinct: true).accessible_by(current_ability, :read).page(params[:page])
  end

  def show
    authorize! :read, @topo
  end

  def new
    @topo = Topo.new
    set_target
    authorize! :create, @topo
  end

  def edit
    authorize! :update, @topo
  end

  def create
    @topo = Topo.new(topo_params)
    set_target
    authorize! :create, @topo

    if @topo.save
      redirect_to @topo.target, notice: _('Topo was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @topo

    if @topo.update(topo_params)
      redirect_to @topo, notice: _('Topo was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @topo

    @topo.destroy
    redirect_to topos_url, notice: _('Topo was successfully destroyed.')
  end

  private

  def set_topo
    @topo = Topo.find(params[:id])
  end

  def set_target
    if params[:geo_ref_id].present?
      @topo.target = GeoRef.find(params[:geo_ref_id])
    end
  end

  def topo_params
    params.require(:topo).permit(:title, :description, :background, :overlay)
  end
end
