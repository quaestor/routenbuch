class SearchController < ApplicationController
  skip_authorization_check only: [:index]

  def index
    @q = params[:q]

    if @q.start_with? '#'
      search_rbid
    else
      full_search
    end
  end

  private

  def search_rbid
    @rbid = Rbid.find_by_rbid(@q)
    redirect_to @rbid.item.as_base_class
  end

  def full_search
    return unless @q.present?

    @geo_refs =
      GeoRef
      .includes(parent: :path_cache) \
      .search_full_text(@q) \
      .accessible_by(current_ability, :read) \
      .page(params['geo_refs_page'])
    @routes =
      Route
      .includes(geo_ref: :path_cache) \
      .search_full_text(@q) \
      .accessible_by(current_ability, :read) \
      .page(params['routes_page'])
    @first_ascent_people =
      FirstAscentPerson
      .search_full_text(@q) \
      .accessible_by(current_ability, :read) \
      .page(params['first_ascent_people_page'])
  end
end
