class Admin::VendorsController < Admin::ApplicationController
  before_action :set_vendor, only: [:show, :edit, :update, :destroy]
  skip_authorization_check :only => [:index, :autocomplete]

  def autocomplete
    @vendors = Vendor \
      .where('name ILIKE (?)', "%#{params[:term]}%") \
      .accessible_by(current_ability, :admin) \
      .order('LENGTH(vendors.name) ASC, vendors.name ASC') \
      .limit(5)
    render(
      json: @vendors.map do |f|
        {
          id: f.id,
          value: f.name,
          label: f.name
        }
      end
    )
  end

  def index
    @q = Vendor.ransack(params[:q])
    @vendors = @q.result \
      .accessible_by(current_ability, :admin) \
      .order(created_at: :desc) \
      .page(params[:page])
  end

  def show
    authorize! :admin, @vendor
    @products = @vendor.products \
      .accessible_by(current_ability, :admin) \
      .order(created_at: :desc) \
      .page(params[:page])
  end

  def new
    @vendor = Vendor.new
    authorize! :admin, @vendor
  end

  def edit
    authorize! :admin, @vendor
  end

  def create
    @vendor = Vendor.new(vendor_params)
    authorize! :admin, @vendor

    if @vendor.save
      redirect_to admin_vendors_path, notice: _('Vendor was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :admin, @vendor

    if @vendor.update(vendor_params)
      redirect_to admin_vendor_path(@vendor), notice: _('Vendor was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :admin, @vendor
    @vendor.destroy
    redirect_to admin_vendors_url, notice: _('Vendor was successfully removed.')
  end

  private

  def set_vendor
    @vendor = Vendor.find(params[:id])
  end

  def vendor_params
    params \
      .require(:vendor) \
      .permit(
        :name,
        :url
      )
  end
end
