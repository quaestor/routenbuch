class Admin::SysInfoController < Admin::ApplicationController
  def index
    @version = Routenbuch.version
    @sys_info = Routenbuch.sys_info
    @supported_image_types = Routenbuch.supported_image_types
  end
end
