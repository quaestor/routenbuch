class DashboardController < ApplicationController
  skip_authorization_check(
    only: %i[
      index
      mark_as_read
      mark_all_as_read
    ]
  )
  before_action :set_timestamp_service
  before_action :set_dashs, only: %i[index]
  before_action :set_versions, only: %i[index versions]

  def index; end

  def versions
    authorize! :read, Version
  end

  def mark_as_read
    @timestamp_service.mark_as_read(params[:kind])
    redirect_to dashboard_path
  end

  def mark_all_as_read
    @timestamp_service.mark_all_as_read
    redirect_to dashboard_path
  end

  private

  def set_timestamp_service
    @timestamp_service = DashboardTimestampService.new(current_user)
  end

  def dash_for(table)
    model = table.classify.constantize
    paginate_param_name = "#{table}_page"
    relation =
      model
      .accessible_by(current_ability, :read)
      .where('created_at > ?', @timestamp_service.timestamp_for(table))
      .order(created_at: :desc)
      .page(params[paginate_param_name])
      .per(10)

    {
      table: table,
      model: model,
      relation: relation,
      paginate_param_name: paginate_param_name
    }
  end

  def set_dashs
    @dashs = %w[
      routes
      geo_refs
      comments
      photos
    ].map do |t|
      dash_for(t)
    end
  end

  def set_versions
    return unless can? :read, Version

    @versions =
      Version
      .accessible_by(current_ability, :read)
      .where('created_at > ?', @timestamp_service.timestamp_for('versions'))
      .includes(:item, :user, :geo_ref, :route)
      .reorder(created_at: :desc)
      .page(params[:page])
  end
end
