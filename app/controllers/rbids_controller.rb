require 'rqrcode'

class RbidsController < ApplicationController
  before_action :set_rbid

  def show
  end

  def qrcode
    qrcode = RQRCode::QRCode.new(rbid_redirect_url(@rbid.rbid))
    svg = qrcode.as_svg(
      color: '000',
      shape_rendering: 'crispEdges',
      module_size: 11,
      standalone: true,
      use_path: true
    )
    respond_to do |format|
      format.svg { render inline: svg }
    end
  end

  def redirect
    redirect_to @rbid.item.as_base_class
  end

  private

  def set_rbid
    @rbid = if params.key? :rbid
                 Rbid.find_by_rbid(params[:rbid])
               else
                 Rbid.find(params[:id])
               end
    authorize! :read, @rbid.item
  end
end
