require 'active_support/concern'

module AdvancedSearchableController
  extend ActiveSupport::Concern

  def search
    @advanced_search = true
    index
    render :index
  end
end
