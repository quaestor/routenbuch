require 'active_support/concern'

module RequireFeatures
  extend ActiveSupport::Concern

  class MissingFeaturesError < StandardError; end

  class_methods do
    def require_feature(*features)
      @required_features ||= []
      @required_features += features
    end

    def required_features
      @required_features || []
    end
  end

  def check_required_features
    missing_features = self.class.required_features.select do |feature|
      !Routenbuch.features.send("#{feature}?")
    end

    if missing_features.any?
      skip_authorization_check
      redirect_to root_path, notice: _('Feature is disabled')
    else
      yield
    end
  end

  included do
    around_action :check_required_features
  end
end
