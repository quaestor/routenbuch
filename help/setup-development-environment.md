## Setting up a person development environment

This document describes how to get started with a personal
development environment using docker on linux.

### Prerequisites

* Linux OS (eg. Debian, Ubuntu, etc.)
* Git client
* Docker
* docker-compose 

On Debian Linux git, docker and docker-compose can be installed from distribution
repositories:

```
apt-get install -y git docker-ce docker-compose
```

### Checkout routenbuch sources

To get the routenbuch source clone the respository to a location of your choice.

```
git clone git@gitlab.com:routenbuch/routenbuch.git ~/development/routenbuch
cd ~/development/routenbuch
```

### Start development instance

To startup development containers:

```
docker-compose up
```

On first startup it will take some time to build the docker containers from the Dockerfile.

After the images have been build the following containers will be started:

* **web** - rails webserver (puma)
* **webpacker** - webpacker server for dynamic assets
* **worker** - sidekiq background job worker
* **redis** - redis database for sidekiq communication
* **database** - postgres database server

After startup the application can be access by browser:

* [Open browser on localhost:3000](http://localhost:3000)

On first startup the database will be empty and the webserver will display a `ActiveRecord::PendingMigrationError` exeption
as the database is still empty at this stage.

### Initialize database

To generate the database tables and initialize it with some basic records execute the `db:setup` action inside the `web` container.

```
docker-compose exec web rails db:setup
```

After refreshing the browser the routenbuch instance should be ready.

### Create initial admin user

To be able to login you must create a user first.

This can be done using the rails console:

```
docker-compose exec web rails console
```

The rails console is a irb (interactive ruby) shell with the rails environment loaded.

To create an admin user use the following code:

```ruby
User.create!(email: 'root@localhost', password: 'secret', role: 'admin')
```

Use an email address and password of your choice.

Log in and have fun!
