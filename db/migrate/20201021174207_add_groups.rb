class AddGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :groups do |t|
      t.string :name, null: false
      t.text :description
      t.timestamps
    end

    create_table :groups_users, id: false do |t|
      t.belongs_to :group
      t.belongs_to :user
    end

    create_table :geo_refs_groups, id: false do |t|
      t.belongs_to :geo_ref
      t.belongs_to :group
    end
  end
end
