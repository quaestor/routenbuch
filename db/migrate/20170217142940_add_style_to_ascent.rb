class AddStyleToAscent < ActiveRecord::Migration[5.0]
  def change
    add_reference :ascents, :style, foreign_key: true
  end
end
