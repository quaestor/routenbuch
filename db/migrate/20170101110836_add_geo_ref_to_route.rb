class AddGeoRefToRoute < ActiveRecord::Migration[5.0]
  def change
    add_reference :routes, :geo_ref, foreign_key: true
  end
end
