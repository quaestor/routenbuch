class AddStartEndActiveToClosures < ActiveRecord::Migration[6.1]
  def change
    add_column :closures, :start_at, :date
    add_column :closures, :end_at, :date
    add_column :closures, :active, :boolean, null: false, default: false
    add_column :closures, :active_changed_at, :date

    up_only do
      Closure.all.each { |c| c.save! }
    end
  end
end
