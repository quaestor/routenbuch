class AddSearchableTerms < ActiveRecord::Migration[6.0]
  def change
    add_column :geo_refs, :searchable_terms, :string, null: false, default: ''
    add_column :routes, :searchable_terms, :string, null: false, default: ''
    add_column :first_ascent_people, :searchable_terms, :string, null: false, default: ''

    up_only do
      GeoRef.all.each(&:save)
      Route.all.each(&:save)
      FirstAscentPerson.all.each(&:save)
    end
  end
end
