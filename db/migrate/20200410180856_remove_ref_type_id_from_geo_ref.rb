class RemoveRefTypeIdFromGeoRef < ActiveRecord::Migration[6.0]
  def change
    remove_column :geo_refs, :geo_ref_type_id
  end
end
