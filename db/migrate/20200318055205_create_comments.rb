class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.string :target_type
      t.integer :target_id

      t.references(:user, index: true)

      t.text :body, null: false, default: ''
      t.string :kind, null:false, default: 'comment'
      t.boolean :private, null: false, default: false

      t.timestamps

      t.index [:target_type, :target_id]
      t.index :kind
      t.index :private
    end
  end
end
