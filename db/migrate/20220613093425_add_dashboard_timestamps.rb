class AddDashboardTimestamps < ActiveRecord::Migration[6.1]
  def change
    create_table :dashboard_timestamps do |t|
      t.references :user, foreign_key: true
      t.string :kind, null: false
      t.datetime :updated_at, null: false
      t.index %i[user_id kind], unique: true
    end
  end
end
