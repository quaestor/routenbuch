class CreateRatings < ActiveRecord::Migration[7.0]
  def change
    create_table :ratings do |t|
      t.string :target_type
      t.integer :target_id

      t.float :average_score
      t.integer :user_ratings_count, null: false, default: 0

      t.index %i[target_type target_id], unique: true
    end

    create_table :user_ratings do |t|
      t.bigint :rating_id, null: false
      t.bigint :user_id, null: false

      t.integer :score, null: false

      t.index %i[rating_id user_id], unique: true
    end
  end
end
