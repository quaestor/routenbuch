class MigrateCarrierwaveToActiveStorage < ActiveRecord::Migration[6.0]
  def up
    rename_column :photos, :photo, :old_photo
    rename_column :users, :avatar, :old_avatar
    rename_column :topos, :background, :old_background
    rename_column :topos, :overlay, :old_overlay
  end
end
