class DropCarrierwaveColumns < ActiveRecord::Migration[6.0]
  def change
    remove_column :photos, :old_photo
    remove_column :users, :old_avatar
    remove_column :topos, :old_background
    remove_column :topos, :old_overlay
  end
end
