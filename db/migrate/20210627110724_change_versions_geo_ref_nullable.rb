class ChangeVersionsGeoRefNullable < ActiveRecord::Migration[6.1]
  def change
    change_column_null :versions, :geo_ref_id, true
  end
end
