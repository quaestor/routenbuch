class RedesignPermissions < ActiveRecord::Migration[6.0]
  def change
    drop_table :groups do |t|
      t.string :name, null: false
      t.text :description
      t.timestamps
    end

    drop_table :groups_users, id: false do |t|
      t.belongs_to :group
      t.belongs_to :user
    end

    drop_table :geo_refs_groups, id: false do |t|
      t.belongs_to :geo_ref
      t.belongs_to :group
    end

    create_table :permissions do |t|
      t.belongs_to :geo_ref
      t.belongs_to :user
      t.string :level, null: false, default: 'editor'
    end
    add_index :permissions, [:geo_ref_id, :user_id], unique: true

    create_table :effective_permissions, id: false do |t|
      t.belongs_to :geo_ref
      t.belongs_to :permission
    end
    add_index :effective_permissions, [:geo_ref_id, :permission_id], unique: true
  end
end
