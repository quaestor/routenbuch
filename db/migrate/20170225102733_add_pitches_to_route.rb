class AddPitchesToRoute < ActiveRecord::Migration[5.0]
  def change
    add_column :routes, :pitches, :integer
  end
end
