class CreateGeoRefPathCache < ActiveRecord::Migration[7.0]
  def change
    create_table :geo_ref_path_caches do |t|
      t.references :geo_ref, null: false, foreign_key: true, index: { unique: true }
      t.jsonb :path

      t.timestamps
    end
  end
end
