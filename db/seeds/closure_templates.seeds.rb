after :closure_reasons do
  [
    [N_('Peregrine falcon'), 'breeding birds', 2, 1, 6, 30],
    [N_('Eagle Owl'), 'breeding birds', 1, 1, 7, 31],
    [N_('Bat'), 'bats', 10, 1, 3, 31],
  ].each do |name, reason, start_month, start_day, end_month, end_day|
    attributes = {
      closure_reason: ClosureReason.find_by(name: reason),
      regular_start_month: start_month,
      regular_start_day_of_month: start_day,
      regular_end_month: end_month,
      regular_end_day_of_month: end_day
    }
    template = ClosureTemplate.find_by(name: name)
    if template.present?
      template.update(**attributes)
    else
      ClosureTemplate.create!(
        name: name,
        **attributes
      )
    end
  end
end
